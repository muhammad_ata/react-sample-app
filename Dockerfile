FROM node:current-alpine
WORKDIR /sample-app
COPY package*.json /sample-app/
RUN npm install
COPY . /sample-app
CMD ["npm", "start"]